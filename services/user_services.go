package services

import (
	"gitlab.com/Bekasu/mvc/domain"
	"gitlab.com/Bekasu/mvc/utils"
)

func GetUser(userId int64) (*domain.User, *utils.ApplicationError) {
	return domain.GetUser(userId)
}
